import { Observer, Subject } from "../../../observer/observer"
import { TimerModel } from "./TimerModel"

export class TimerView {

    private _TimerModelObserver: Observer
    private _timePara: HTMLParagraphElement
    constructor() {

        this._timePara = document.createElement('p')
        this._TimerModelObserver = new Observer((timerModel: TimerModel) => {
            this.render(timerModel)
        })
    }

    render(timerModel: TimerModel) {
        this._timePara.innerText = timerModel.timer.toString()
    }

    get timerObserver() {
        return this._TimerModelObserver
    }

    get main() {
        return this._timePara
    }

}