import { Observer } from "../../../observer/observer";
import { TimerModel } from "./TimerModel";
import { TimerView } from "./TimerView";
import { Router } from '../../../router/Router';

export class TimerController {

    private _timerModel: TimerModel
    private _timerView: TimerView

    constructor() {
        this._timerModel = new TimerModel();
        this._timerView = new TimerView();
        this._timerModel.timerModelSubject.subscribe(this._timerView.timerObserver)
    }

    get timerModel(): TimerModel { return this._timerModel}
    get view(): TimerView { return this._timerView}

    init() {
        this._timerView.render(this.timerModel)
    }

    update(){
        // Router.instance.changeRoute('')
    }
}