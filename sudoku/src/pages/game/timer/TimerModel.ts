import { Subject } from "../../../observer/observer";

export class TimerModel {
    
    private _interval: number;
    private _fullTime: number
    private _timerSubject: Subject

    constructor(time:number = 1000) {

        this._timerSubject = new Subject();

        this._fullTime = 0
        this._interval = setInterval(() => {
            this._fullTime ++
            this._timerSubject.notify(this)
        }, time)
    }

    get timer(): number {
        return this._fullTime
    }

    get timerModelSubject() {
        return this._timerSubject
    }

}