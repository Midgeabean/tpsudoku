import { Observer, Subject } from "../../../../observer/observer";

export class squareModel {

    private _squareSubject: Subject
    private _valeur: number | null
       

    constructor(valeur: number ) {

        this._valeur = valeur == -1 ? null : valeur
        this._squareSubject = new Subject();

    }

        get valeur(): number {
            return this._valeur!

        }

        set valeur(valeur: number) {
            this._valeur = valeur
            this._squareSubject.notify(this)        
      }

      get modelSubject(): Subject {
        return this._squareSubject
      } 
}