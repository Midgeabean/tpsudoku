import { Observer, Subject } from "../../../../observer/observer";
import { squareModel } from './SquareModel';


export class squareView {

    private _squareSubject: Subject
    private _input : HTMLInputElement
    private _squareObserver: Observer

    constructor() {

        this._input = document.createElement("input")
        this._input.classList.add("case")
        this._squareSubject = new Subject();
        this._squareObserver = new Observer((squareModel: squareModel) =>{
            this.render(squareModel)
        })
        this._input.addEventListener("keyup",()=>{
            this._squareSubject.notify(this._input.value)
        })

    }

        render(squareModel: squareModel) {
            this._input.value = squareModel.valeur == null ? "" : squareModel.valeur.toString()
        }

        get main(): HTMLInputElement {
            return this._input 

        }

        get observerView(): Observer {
            return this._squareObserver
        }

}