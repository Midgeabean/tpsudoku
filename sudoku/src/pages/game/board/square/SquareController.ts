import { Observer } from "../../../../observer/observer";
import { squareModel } from "./SquareModel"
import { squareView } from './SquareView';

export class squareController{

    private _squareModel : squareModel
    private _squareView : squareView
    private _squareViewObserver : Observer


    constructor(value: number = -1){
        this._squareModel = new squareModel(value);
        this._squareView = new squareView()
        this._squareModel.modelSubject.subscribe(this._squareView.observerView)
        this._squareViewObserver = new Observer((valeur : string) =>{
            this._squareModel.valeur = parseInt(valeur)
        })
    }

    init(){
        this._squareView.render(this._squareModel)

    }

    get view():squareView {
        return this._squareView
    } 
    
    get model(): squareModel {
        return this._squareModel
    }


}