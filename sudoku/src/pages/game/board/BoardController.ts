import { BoardModel } from "./BoardModel";
import { boardView } from "./BoardView";
import { squareController } from "./square/SquareController";

export class BoardController{

    private _boardModel: BoardModel
    private _boardView: boardView
    private _case1: squareController
    private _case2: squareController
    private _case3: squareController
    private _case4: squareController
    private _case5: squareController
    private _case6: squareController
    private _case7: squareController
    private _case8: squareController
    private _case9: squareController
    private _case10: squareController
    private _case11: squareController
    private _case12: squareController
    private _case13: squareController
    private _case14: squareController
    private _case15: squareController
    private _case16: squareController

    constructor() {
        this._case1 = new squareController(2)
        this._case2 = new squareController()
        this._case3 = new squareController()
        this._case4 = new squareController(1)
        this._case5 = new squareController()
        this._case6 = new squareController(4)
        this._case7 = new squareController()
        this._case8 = new squareController()
        this._case9 = new squareController()
        this._case10 = new squareController()
        this._case11 = new squareController(1)
        this._case12 = new squareController()
        this._case13 = new squareController(4)
        this._case14 = new squareController()
        this._case15 = new squareController()
        this._case16 = new squareController(3)

        this._boardModel = new BoardModel(
            this._case1.model,
            this._case2.model,
            this._case3.model,
            this._case4.model,
            this._case5.model,
            this._case6.model,
            this._case7.model,
            this._case8.model,
            this._case9.model,
            this._case10.model,
            this._case11.model,
            this._case12.model,
            this._case13.model,
            this._case14.model,
            this._case15.model,
            this._case16.model
        )
        this._boardView = new boardView(
            this._case1.view,
            this._case2.view,
            this._case3.view,
            this._case4.view,
            this._case5.view,
            this._case6.view,
            this._case7.view,
            this._case8.view,
            this._case9.view,
            this._case10.view,
            this._case11.view,
            this._case12.view,
            this._case13.view,
            this._case14.view,
            this._case15.view,
            this._case16.view
        )
        
    }

    init() {
        this._boardView.render(this._boardModel)
    }

    get view(): boardView {
        return this._boardView
    }

    get model(): BoardModel {
        return this._boardModel
    }

}