import { Subject } from "../../../observer/observer";
import { squareModel } from "./square/SquareModel";

export class BoardModel {

    private _boardModel: Subject
    private _grid: Map<number, squareModel>


    constructor(...squareModel: squareModel[]){
        this._boardModel = new Subject();
        this._grid = new Map<number, squareModel>([
            [ 1, squareModel[0]], 
            [ 2, squareModel[1]], 
            [ 3, squareModel[2]], 
            [ 4, squareModel[3]], 
            [ 5, squareModel[4]], 
            [ 6, squareModel[5]], 
            [ 7, squareModel[6]], 
            [ 8, squareModel[7]], 
            [ 9, squareModel[8]], 
            [ 10, squareModel[9]], 
            [ 11, squareModel[10]], 
            [ 12, squareModel[11]], 
            [ 13, squareModel[12]], 
            [ 14, squareModel[13]], 
            [ 15, squareModel[14]], 
            [ 16, squareModel[15]], 
        ]);
    }

    get modelSubject(): Subject {
        return this._boardModel
    }

    setCaseValue(caseIndex: number, value: number) {
        this._grid.get(caseIndex)!.valeur = value;
    }

    get grid(): Map<number, squareModel> {
        return this._grid
    }
}