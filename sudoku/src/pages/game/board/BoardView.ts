import { Observer, Subject } from "../../../observer/observer"
import { BoardModel } from "./BoardModel";
import { squareView } from './square/SquareView';

export class boardView {

    //private _boardiewSujet: Subject
    private _boardModelObserver: Observer
    private _grid: Map<number, squareView>
    private _mainDiv: HTMLDivElement

    constructor(...squareView: squareView[]) {
      //this._boardiewSujet = new Subject();
      this._mainDiv = document.createElement('div');
      this._boardModelObserver = new Observer((boardModel: BoardModel) => {
        this.render(boardModel);
      });

      this._grid = new Map<number, squareView>([
        [ 1, squareView[0]], 
        [ 2, squareView[1]], 
        [ 3, squareView[2]], 
        [ 4, squareView[3]], 
        [ 5, squareView[4]], 
        [ 6, squareView[5]], 
        [ 7, squareView[6]], 
        [ 8, squareView[7]], 
        [ 9, squareView[8]], 
        [ 10, squareView[9]], 
        [ 11, squareView[10]], 
        [ 12, squareView[11]], 
        [ 13, squareView[12]], 
        [ 14, squareView[13]], 
        [ 15, squareView[14]], 
        [ 16, squareView[15]], 
    ]);
    }

    get main(): HTMLDivElement {
        return this._mainDiv
    }

    render(boardModel: BoardModel) {
        this._grid.forEach((view, key) => {
            this._mainDiv.appendChild(view.main)
            if(key % 4 == 0) {
                this._mainDiv.appendChild(document.createElement("br"))
            }
            view.render(boardModel.grid.get(key)!);
        })
    }

}