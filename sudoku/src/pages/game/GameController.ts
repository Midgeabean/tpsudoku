import { BoardController } from './board/BoardController';
import { GameModel } from './GameModel';
import { GameView } from './GameView';
import { TimerController } from './timer/TimerController';

export class GameController {
    private _gameView: GameView
    private _gameModel: GameModel
    private _boardController: BoardController
    private _timerController: TimerController

    constructor() {
        this._boardController = new BoardController()
        this._timerController = new TimerController()
        this._gameModel = new GameModel(this._boardController.model, this._timerController.timerModel)
        this._gameView = new GameView(this._boardController.view, this._timerController.view)
    }

    get view(): GameView {
        return this._gameView
    }

    init() {
        this._gameView.render(this._gameModel)
    }
}