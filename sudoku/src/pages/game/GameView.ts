//import { Observer, Subject } from "../../observer/observer";
import { boardView } from "./board/BoardView";
import { GameModel } from "./GameModel";
import { TimerView } from "./timer/TimerView";

export class GameView {

    //private _gameModelObserver: Observer
    private _mainDiv: HTMLDivElement
    private _boardView: boardView
    private _timerView: TimerView

    constructor(boardView: boardView, timerView: TimerView) {
        this._mainDiv = document.createElement("div")
        this._boardView = boardView
        this._timerView = timerView
    }

    get main(): HTMLDivElement {
        return this._mainDiv
    }


    render(gameModel: GameModel) {
        this._mainDiv.appendChild(this._timerView.main)
        this._timerView.render(gameModel.timerModel)
        this._mainDiv.appendChild(this._boardView.main)
        this._boardView.render(gameModel.boardModel)
    }


}