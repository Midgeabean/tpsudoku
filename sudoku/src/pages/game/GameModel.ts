import { BoardModel } from "./board/BoardModel";
import { TimerModel } from "./timer/TimerModel";

export class GameModel {
    private _boardModel: BoardModel
    private _timerModel: TimerModel

    constructor(boardModel: BoardModel, timerModel: TimerModel) {
        this._boardModel = boardModel;
        this._timerModel = timerModel;
    }

    get timerModel(): TimerModel {
        return this._timerModel;
    }

    get boardModel(): BoardModel {
        return this._boardModel;
    }
}