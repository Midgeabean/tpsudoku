import { Observer, Subject } from "../../observer/observer";
import { ScoreModel } from "./ScoreModel";

export class ScoreView {

    private _mainDiv: HTMLDivElement;
    private _scoreModelObserver: Observer;
    private _scoreViewSujet: Subject;

    constructor() {
        this._mainDiv = document.createElement('div')

        // SUJET // OBSERVER LIER AU MVC
        this._scoreModelObserver = new Observer((scoreModel: ScoreModel) => {
        this.renderView(scoreModel)
        })

        this._scoreViewSujet = new Subject()
    }

    get scoreViewSujet(): Subject { return this._scoreViewSujet }
    get scoreModelObserver(): Observer { return this._scoreModelObserver }
    get main(): HTMLDivElement{ return this._mainDiv }

    async renderView(scoreModel: ScoreModel){
        let scores: any[] = await scoreModel.getAllScore()

        for(let i = 0; i < scores.length; i++) {
            this._mainDiv.innerHTML += `
                <p>${scores[i].player} : ${scores[i].score} sec</p>
            `
        }

    }

}
