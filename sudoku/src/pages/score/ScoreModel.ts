import { Subject } from "../../observer/observer";


export class ScoreModel {

    private _scoreModelSubject: Subject;

    constructor() {
        this._scoreModelSubject = new Subject()

    }
  // GETTERS || SETTERS
    get titleModelSubject(): Subject { return this._scoreModelSubject }

    async getScore() { return await this.getAllScore() }

    getAllScore() {
        return new Promise<any[]>((resolve, reject) => {

            let xml = new XMLHttpRequest()

            xml.open('GET', "http://localhost:8080/", true)
            xml.send()

            xml.onload = () => {
                resolve(JSON.parse(xml.responseText))
            }
        })
  }
}

