import { ScoreModel } from "./ScoreModel";
import { ScoreView } from "./ScoreView";

export class ScoreController {

    private _scoreModel: ScoreModel;
    private _scoreView: ScoreView;

    constructor() {

        this._scoreModel = new ScoreModel()
        this._scoreView = new ScoreView()


        this._scoreView.renderView(this._scoreModel)
    }

    init() {
        this._scoreView.renderView(this._scoreModel)
    }

    get scoreView(): ScoreModel { return this._scoreModel }
    get view(): ScoreView { return this._scoreView }

}
