import { v4 } from 'uuid'

// L'observer écoute les notfication du sujet
export class Observer {
  private _id: string
  private _function: Function;

  constructor(localFunction: Function = () => {}) {
    this._id = v4()
    this._function = localFunction
  }

  get id(): string { return this._id }

  update(data: any){
    // console.log(Observer [${this._id}] : ${data})
    this._function(data)
  }
}

// Le sujet notifie l'observer de ses modification
export class Subject {

  private _observers: Map<string, Observer>
  private _id: string;

  constructor() {
    this._id = v4()
    this._observers = new Map<string, Observer>()
  }

  notify(data: any){
    // console.log(Sujet [${this._id}] : ${data})
    this._observers.forEach((observer) => {
      observer.update(data)
    })
  }

  subscribe(observer: Observer){
    this._observers.set(observer.id, observer)
  }

  unSubscrive(observer: Observer){
    this._observers.delete(observer.id)
  }

}