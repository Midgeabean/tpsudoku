import { GameController } from './pages/game/GameController'
import { ScoreController } from './pages/score/ScoreController'
import { Router } from './router/Router'
import './style.css'

const router = Router.instance
router.addRoute({path : 'score', component: new ScoreController()})
router.addRoute({path : 'square', component: new GameController()})


router.loadRoute()

