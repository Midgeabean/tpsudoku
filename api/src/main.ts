import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import { BDDConnection } from './bdd/connectionBDD';

const app = express()

app.use(bodyParser.json())
app.use(cors())

app.post('/registerscore', (req, res) => {
    let score = req.body.score
    let player = req.body.player

    BDDConnection.getConnection().registerScore(player, score)
    res.send()
})

app.get('/', async (req, res) => {
    res.json(await BDDConnection.getConnection().getAllScore())
})

app.listen(8080)
