import mysql from 'mysql'
import { v4 } from 'uuid';

export class BDDConnection {

    private static _instance: BDDConnection
    private _connection: any

    constructor(){
        this._connection = mysql.createConnection({
            host     : '127.0.0.1',
            port     :  3306,
            user     : 'root',
            password : '',
            database : 'sudoku'
        });

        // this._connection.connect((err:any) => {
        //     if (err) {
        //         console.error('error connecting: ' + err.stack);
        //         return;
        //     }
        //     console.log('connected as id ' + this._connection.threadId);
        // });

    }

    static getConnection(): BDDConnection {
        if(BDDConnection._instance == null){
            BDDConnection._instance = new BDDConnection()
        }
        return BDDConnection._instance
    }

    registerScore(pseudo: string, time: number) {
        this._connection.query(`INSERT INTO score (id, player, score) VALUES ("${v4()}", "${pseudo}", ${time})`)
    }

    getAllScore(){
        return new Promise<any>((resolve, reject) => {
            this._connection.query(`SELECT * FROM score ORDER BY score ASC`, (err:any, result:any) => {
                if(err) throw err
                resolve(result)
            })
        })
    }
}